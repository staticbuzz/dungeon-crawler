#ifndef GAME_C
#define GAME_C

#include "Game.h"
#include "DungeonCoord.h"
#include "Character.h"
#include "Monster.h"
#include "Hero.h"
#include "Item.h"
#include "DungeonObjFactory.h"
#include <vector>
#include <algorithm>
#include <windows.h>
#include <stdlib.h>
#include <sstream>
#include <ctime>

Game::Game()
{
	console = GetConsoleWindow();
	srand((unsigned)time(NULL));
	 

	//create DungeonCoord pointers into theDungeonGrid[][]
	int exitRandNum = rand() % 10+1;
	DungeonCoord* theExit;
	for (int i = 0; i < 22; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			theDungeonGrid[i][j] = new DungeonCoord;
			theDungeonGrid[i][j]->xPos = i;
			theDungeonGrid[i][j]->yPos = j;
			if (i == 0 || (i == 21 && j != exitRandNum) || j == 0 || j == 11)
				theDungeonGrid[i][j]->isWall = true;
			else if (i == 21 && j == exitRandNum)
			{
				//assign a name to the randomly-determined DungeonCoord of the exit so we can place boss monster later
				theExit = theDungeonGrid[i][j];
				theExit->isExit = true;
			}
		}

	}

	spawnObjects();
	for (unsigned i = 1; i < allTheThings.size(); i++)
	{
		std::cout << allTheThings.at(i)->displayInfo() << std::endl;
		//std::cout << allTheThings.at(i)->getName() << std::endl;
		std::cout << allTheThings.at(i)->getCoord()->xPos << "   " << allTheThings.at(i)->getCoord()->yPos << std::endl;
	}
	
	//draw the dungeon
	std::cout << std::endl;
	for (int j = 0; j < 12; j++)
	{
		for (int i = 0; i < 22; i++)
		{
			if (theDungeonGrid[i][j]->holdsMonster && theDungeonGrid[i][j]->isVisible)
				std::cout << "@";
			else if (theDungeonGrid[i][j]->holdsItem  && theDungeonGrid[i][j]->isVisible)
				std::cout << "+";
			else if (theDungeonGrid[i][j]->holdsHero)
				std::cout << char(23);
			else if ((theDungeonGrid[i][j]->yPos == 0 || theDungeonGrid[i][j]->yPos == 11) && theDungeonGrid[i][j]->isVisible)
			{
				if (theDungeonGrid[i][j]->xPos == 0 && theDungeonGrid[i][j]->yPos == 0)
					std::cout << char(201);
				else if (theDungeonGrid[i][j]->xPos == 21 && theDungeonGrid[i][j]->yPos == 0)
					std::cout << char(187);
				else if (theDungeonGrid[i][j]->xPos == 0 && theDungeonGrid[i][j]->yPos == 11)
					std::cout << char(200);
				else if (theDungeonGrid[i][j]->xPos == 21 && theDungeonGrid[i][j]->yPos == 11)
					std::cout << char(188);
				else
					std::cout << char(205);
			}
				
			else if ((theDungeonGrid[i][j]->xPos == 0 || theDungeonGrid[i][j]->xPos == 21) && theDungeonGrid[i][j]->isVisible && !theDungeonGrid[i][j]->isExit)
				std::cout << char(186);
			else
			{
				if (theDungeonGrid[i][j]->isVisible)
					std::cout << " ";
				else
					std::cout << char(178);
			}
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	system("pause");
}

//change the console text colour
void Game::setTextColour(int txtColour)
{
	txtColour = (Colour)txtColour;
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hcon, txtColour);
}

//create the objects(monsters, items, hero) and their DungeonCoords
void Game::spawnObjects()
{
	
	int randNum;
	//create pointer to a DungeonCoord struct for setting monster positions in the dungeon
	DungeonCoord* tempCoord;
	randNum = rand() % 10+1;
	tempCoord = new DungeonCoord;
	tempCoord->xPos = 1;
	tempCoord->yPos = randNum;
	tempCoord->holdsHero = true;
	allTheThings.push_back(new Hero(tempCoord));
	delete theDungeonGrid[1][randNum];
	theDungeonGrid[1][randNum] = tempCoord;
	//create dungeon object factory
	DungeonObjFactory *theFactory = new DungeonObjFactory();
	Character* monster;
	Item* item;
	//create 5 monsters using MonsterFactory, and each time pass a pointer to a random DungeonCoord.
	for (int i = 0; i < 10; i++)
	{
		int tempX, tempY;
		bool isOccupied = true;
		tempCoord = new DungeonCoord;
		//this loop prevents two game objects being created in the same DungeonCoord via isOccupied boolean
		while (isOccupied)
		{
			randNum = rand() % 20+1;
			tempX = randNum;
			//set the x-position of the temp DungeonCoord pointer
			tempCoord->xPos = tempX;
			randNum = rand() % 10+1;
			tempY = randNum;
			//set the y-position of the temp DungeonCoord pointer
			tempCoord->yPos = tempY;
			//indicate that the DungeonCoord holds an object (used to prevent multiple monsters/items/hero landing on same square when game starts)
			if (i < 5)
				tempCoord->holdsMonster = true;
			else
				tempCoord->holdsItem = true;

			if (!theDungeonGrid[tempX][tempY]->holdsMonster && !theDungeonGrid[tempX][tempY]->holdsItem && !theDungeonGrid[tempX][tempY]->holdsHero)
				isOccupied = false;
		}
		//we are sure DungeonCoord is empty so now replace the appropriate DungeonCoord from theDungeonGrid with this temp one
		delete theDungeonGrid[tempX][tempY];
		theDungeonGrid[tempX][tempY] = tempCoord;
		if (i < 5)
		{
			//pick a random number to give to the MonsterFactory (used to select which child class of Monster to create)
			randNum = rand() % 5 + 1;
			//create a monster and return a pointer to it
			monster = theFactory->getMonster(randNum, tempCoord);
			//add the monster pointer to the theCharacters vector
			allTheThings.push_back(monster);
			monster = nullptr;
		}
		else
		{
			//pick a random number to give to the ItemFactory (used to select which child class of Item to create)
			randNum = rand() % 5 + 1;
			//create a item and return a pointer to it
			//////item = DungeonObjFactory->getItem(randNum, tempCoord);
			item = new Item("An item of some kind", tempCoord);
			//add the item pointer to the theCharacters vector
			allTheThings.push_back(item);
			item = nullptr;
		}
		tempCoord = nullptr;
	}
}


#endif