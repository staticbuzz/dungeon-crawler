#ifndef DUNGEON_OBJ_H
#define DUNGEON_OBJ_H
#include <string>
#include <sstream>
#include "DungeonCoord.h"

class DungeonObject {
public:
	DungeonObject();
	DungeonCoord* getCoord();
	virtual std::string displayInfo() = 0;
	virtual ~DungeonObject();
protected:
	//position in the dungeon
	DungeonCoord* dungeonCoord;
};


#endif