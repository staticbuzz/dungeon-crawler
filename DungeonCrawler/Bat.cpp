#ifndef BAT_C
#define BAT_C

#include "Character.h"
#include "Monster.h"
#include "Bat.h"


Bat::Bat(std::string _name, DungeonCoord* _coord) : Monster()
{
	name = _name;
	dungeonCoord = _coord;
}

std::string Bat::speak()
{
	std::string speak;
	speak = "\n   Screech chirrp! Blooooood!";
	return speak;
}

Bat::~Bat()
{

}


#endif