#ifndef MONSTER_C
#define MONSTER_C
#include "Monster.h"
#include "Character.h"
#include <string>
#include <sstream>
#include <iostream>
#include <vector>


Monster::Monster() : Character()
{
	attackLevel = rand() % 7 + 4;
	defenceLevel = rand() % 7 + 4;
	maxHealth = rand() % 9 + 5;
	currentHealth = maxHealth;
	agility = rand() % 15 + 5;
}


std::string Monster::displayInfo()
{
	std::stringstream ss;
	ss << "\nSpecies: " << name; 
	ss << Character::displayInfo();
	return ss.str();
}

Monster::~Monster()
{
	//
}
#endif