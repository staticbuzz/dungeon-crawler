#ifndef CHARACTER_C
#define CHARACTER_C
#include <sstream>
#include <string>
#include "Character.h"
#include "DungeonObject.h"

Character::Character() : DungeonObject()
{
	//
}

int Character::attack()
{
	return attackLevel;
}

void Character::takeDamage(int _damage)
{
	currentHealth -= _damage;
}


std::string Character::getName()
{
	return name;
}

/*even though this is a pure virtual function, I am still implementing it here. The child classes' overridden displayStatus()
methods explicitly call this base method for common functionality (much like super in Java...). This is legal in C++*/
std::string Character::displayInfo()
{
	std::stringstream ss;
	ss << "\nCurrent health: " << currentHealth << " / " << maxHealth;
	ss << "\nAttack: " << attackLevel;
	ss << "\nArmour: " << defenceLevel;
	ss << "\nAgility: " << agility;
	return ss.str();
}

Character::~Character()
{
	//
}

#endif