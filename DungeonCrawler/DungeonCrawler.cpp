//DungeonCrawler.cpp
#ifndef DC_C
#define DC_C

#include "DungeonCrawler.h"
//#include "Game.h"


int main(){
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r); //stores the console's current dimensions
	MoveWindow(console, 0, 0, 800, 800, TRUE);

	showWelcome();
	std::cout << "\n                        ";
	system("pause");

	while (true)
	{
		char playAgain;
		Game game;
		system("cls"); 
		showWelcome();
		std::cout << "\n                   Would you like to play again? (y/n) ";
		std::cin >> playAgain;
		if (playAgain == 'n')
		{
			return 0;
		}
	}

	system("pause");

	return 0;
}

void showWelcome()
{
	std::ostringstream greet;
	system("cls");

	Game::setTextColour(14);
	std::cout << "\n\n                          WELCOME TO THE DUNGEON... \n                      OH MY, THE HORRORS THAT AWAIT YOU\n\n\n";
	Game::setTextColour(12);

	greet << "  X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X\n";
	greet << "  |                           ,,'``````````````',,                            |\n";
	greet << "  X                        ,'`                   `',                          X\n";
	greet << "  |                      ,'                         ',                        |\n";
	greet << "  X                    ,'          ;       ;          ',                      X\n";
	greet << "  |       (           ;             ;     ;             ;     (               |\n";
	greet << "  X        )         ;              ;     ;              ;     )              X\n";
	greet << "  |       (         ;                ;   ;                ;   (               |\n";
	greet << "  X        )    ;   ;    ,,'```',,,   ; ;   ,,,'```',,    ;   ;               X\n";
	greet << "  |       (    ; ',;   '`          `',   ,'`          `'   ;,' ;              |\n";
	greet << "  X        )  ; ;`,`',  _--~~~~--__   ' '   __--~~~~--_  ,'`,'; ;     )       X\n";
	greet << "  |       (    ; `,' ; :  /       \\~~-___-~~/       \\  : ; ',' ;     (        |\n";
	greet << "  X  )     )   )',  ;   -_\\  o    /  '   '  \\    o  /_-   ;  ,'       )   (   X\n";
	greet << "  | (     (   (   `;      ~-____--~'       '~--____-~      ;'  )     (     )  |\n";
	greet << "  X  )     )   )   ;            ,`;,,,   ,,,;',            ;  (       )   (   X\n";
	greet << "  | (     (   (  .  ;        ,'`  (__ '_' __)  `',        ;  . )     (     )  |\n";
	greet << "  X  )     \\/ ,\".). ';    ,'`        ~~ ~~       `' ,    ; .(.\", \\/  )    (   X\n";
	greet << "  | (   , ,'|// / (/ ,;  '        _--~~-~~--_        '  ;, \\)    \\|', ,    )  |\n";
	greet << "  X ,)  , \\/ \\|  \\\\,/  ;;       ,; |_| | |_| ;,       ;;  \\,//  |/ \\/ ,   ,   X\n";
	greet << "  | \",   .| \\_ |\\/ |#\\_/;      ;_| : `~'~' : |_;        ;\\_/#| \\/| _/ |.   ,\" |\n";
	greet << "  X#(,'  )  \\\\\\#\\ \\##/)#;     :  `\\/       \\/   :     ;#(\\##/ /#///  (  ',)# ,X\n";
	greet << "  || ) | \\ |/ /#/ |#( \\; ;     :               ;     ; ;/ )#| \\#\\ \\| / | ( |) |\n";
	greet << "  X\\ |.\\\\ |\\_/#| /#),,`   ;     ;./\\_     _/\\.;     ;   `,,(#\\ |#\\_/| //.| / ,X\n";
	greet << "  | \\\\_/# |#\\##/,,'`       ;     ~~--|~|~|--~~     ;       `',,\\##/#| #\\_// \\/|\n";
	greet << "  X  ##/#  #,,'`            ;        ~~~~~        ;            `',,#  #\\##  //X\n";
	greet << "  |####@,,'`                 `',               ,'`                 `',,@####| |\n";
	greet << "  X#,,'`                        `',         ,'`                        `',,###X\n";
	greet << "  |'                               ~~-----~~                               `' |\n";
	greet << "  X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X \n";


	/*Game::setTextColour(6);
	greet << "                    (    )         \n";
	greet << "                   ((((()))        \n";
	greet << "                   |o\\ /o)|        \n";
	greet << "                   ( (  _')        \n";
	greet << "                   ( ._.  /\\__     \n";
	greet << "                  , \\___,/'  ')    \n";
	greet << "     .,_,,       (  .- .   .    )  \n";
	greet << "     \\   \\\\     ('        )(    )  \n";
	greet << "      \\   \\\\    \\. _. __ ___( .  | \n";
	greet << "       \\  /\\\\   .(  .'  /\\  '.  )  \n";
	greet << "        \\(  \\\\.-' (/    \\/    \\)   \n";
	greet << "         '  ())_'.-| /\\ /\\ /\\ |    \n";
	greet << "               \\\\.(| \\/ \\/ \\/ |     \n";
	*/


	std::cout << greet.str();
	Game::setTextColour(7);
}






#endif









