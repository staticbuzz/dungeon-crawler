#ifndef TOAD_H
#define TOAD_H
#include <string>

class Toad : public Monster{
public:
	Toad(std::string _name, DungeonCoord* _coord);
	std::string speak();
	~Toad();
};


#endif