#ifndef BAT_H
#define BAT_H
#include <string>


class Bat : public Monster{
public:
	Bat(std::string _name, DungeonCoord* _coord);
	std::string speak();
	~Bat();
};


#endif