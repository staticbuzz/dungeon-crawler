#ifndef ITEM_C
#define ITEM_C
#include "Item.h"
#include "DungeonObject.h"


Item::Item(std::string _desc, DungeonCoord* _coord) : DungeonObject()
{
	description = _desc;
	dungeonCoord = _coord;
}

/*even though this is a pure virtual function, I am still implementing it here. The child classes' overridden displayInfo()
methods explicitly call this base method for common functionality (much like super in Java...). This is legal in C++*/
std::string Item::displayInfo()
{
	std::stringstream ss;
	ss << "\nDescription: " << description;
	ss << "\nWorth: " << worth << " gold pieces";
	return ss.str();
}

std::string Item::getDescription()
{
	return description;
}


Item::~Item()
{
	//
}

#endif