#ifndef HERO_H
#define HERO_H
#include <string>
#include <vector>
#include "DungeonCoord.h"

class Hero : public Character{
public:
	Hero(DungeonCoord* _coord);
	std::string displayInfo();
	std::string speak();
	~Hero();
private:
	int charisma, XP;
	std::vector<Item*> inventory;
	bool isPoisoned, isDiseased, isBleeding;
};


#endif