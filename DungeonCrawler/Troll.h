#ifndef TROLL_H
#define TROLL_H
#include <string>

class Troll : public Monster{
public:
	Troll(std::string _name, DungeonCoord* _coord);
	std::string speak();
	~Troll();
};


#endif