#ifndef CORPSE_H
#define CORPSE_H
#include <string>



class Corpse : public Monster{
public:
	Corpse(std::string _name, DungeonCoord* _coord);
	std::string speak();
	~Corpse();
};


#endif