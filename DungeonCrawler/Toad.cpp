#ifndef TOAD_C
#define TOAD_C

#include "Character.h"
#include "Monster.h"
#include "Toad.h"

Toad::Toad(std::string _name, DungeonCoord* _coord) : Monster()
{
	name = _name;
	dungeonCoord = _coord;
}

std::string Toad::speak()
{
	std::string speak;
	speak = "\n   Ribbit croak croak... Garrrrgump!";
	return speak;
}

Toad::~Toad()
{

}

#endif