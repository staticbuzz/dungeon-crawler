#ifndef DUNGOBJFACT_H
#define DUNGOBJFACT_H

#include "Monster.h"
#include "Item.h"
#include "DungeonCoord.h"

/*Dungeon Object Factory Class*/
class DungeonObjFactory
{
public:
	/*Factory Methods*/
	Monster* getMonster(int _monsterType, DungeonCoord* _coord);
	Item* getItem(int _itemType, int _condition, int _quality, DungeonCoord* _coord);
private:
	enum e_Monster{ BAT = 1, CORPSE, TOAD, TROLL, WARLOCK, BOSS };
	enum e_Item{ SWORD = 1, SHIELD, AXE, DAGGER, STAFF, CHAINARMOUR, LEATHERVEST, HELMET, BOOTS, POTION, TREASURE, AMULET };
};

#endif