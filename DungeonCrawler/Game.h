#ifndef GAME_H
#define GAME_H

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <windows.h>
#include <stdlib.h>
#include <sstream>
#include <ctime>
#include "DungeonCoord.h"
#include "Item.h"
#include "Character.h"
class Game
{
public:
	Game();
	static void setTextColour(int txtColour);
	void spawnObjects();
	/*void populateItems();
	void drawDungeon();
	void toggleCheatMode();
	*/


private:
	HWND console;
	enum Colour { DBLUE = 1, GREEN, GREY, DRED, DPURP, BROWN, LGREY, DGREY, BLUE, LIMEG, TEAL, RED, PURPLE, YELLOW, WHITE };
	DungeonCoord* theDungeonGrid[22][12];  //[posX][posY] - the 1st & 22st posX columns are used only to draw the vertical walls (including the exit).
	std::vector<DungeonObject*> allTheThings;

};

#endif
