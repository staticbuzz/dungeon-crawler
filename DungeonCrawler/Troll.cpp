#ifndef TROLL_C
#define TROLL_C

#include "Character.h"
#include "Monster.h"
#include "Troll.h"


Troll::Troll(std::string _name, DungeonCoord* _coord) : Monster()
{
	name = _name;
	dungeonCoord = _coord;
}

std::string Troll::speak()
{
	std::string speak;
	speak = "\n   Derrrrp bones make good crunch, yum...";
	return speak;
}

Troll::~Troll()
{

}
#endif