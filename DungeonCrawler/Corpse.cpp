#ifndef CORPSE_C
#define CORPSE_C

#include "Character.h"
#include "Monster.h"
#include "Corpse.h"


Corpse::Corpse(std::string _name, DungeonCoord* _coord) : Monster()
{
	name = _name;
	dungeonCoord = _coord;
}

std::string Corpse::speak()
{
	std::string speak;
	speak = "\n   Grooooooaaaaaaannnnnn";
	return speak;
}

Corpse::~Corpse()
{

}


#endif