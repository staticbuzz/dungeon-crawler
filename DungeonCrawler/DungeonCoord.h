#ifndef DUNGEONCOORD_H
#define DUNGEONCOORD_H

struct DungeonCoord {
	int xPos, yPos;
	bool holdsMonster, holdsItem, holdsHero, isWall, isExit, isVisible;
	//initialisation list to set the five booleans to false
	DungeonCoord() : holdsHero(false), holdsItem(false), holdsMonster(false), isWall(false), isExit(false), isVisible(true){}
};

#endif