#ifndef WARLOCK_C
#define WARLOCK_C

#include "Character.h"
#include "Monster.h"
#include "Warlock.h"

Warlock::Warlock(std::string _name, DungeonCoord* _coord) : Monster()
{
	name = _name;
	dungeonCoord = _coord;
}

std::string Warlock::speak()
{
	std::string speak;
	speak = "\n   My wand will turn you into a frog. Then I'll step on you!";
	return speak;
}

Warlock::~Warlock()
{

}
#endif