#ifndef WARLOCK_H
#define WARLOCK_H
#include <string>

class Warlock : public Monster{
public:
	Warlock(std::string _name, DungeonCoord* _coord);
	std::string speak();
	~Warlock();
}; 


#endif