#ifndef MONSTER_H
#define MONSTER_H
#include <string>
#include "Character.h"
#include "DungeonCoord.h"

class Monster : public Character{
public:
	Monster();
	std::string displayInfo();
	virtual std::string speak() = 0;
	virtual ~Monster();
protected:
	std::string species;
};


#endif