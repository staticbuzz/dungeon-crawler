#ifndef HERO_C
#define HERO_C
#include "DungeonCoord.h"
#include "Character.h"
#include "Item.h"
#include "Hero.h"
#include <string>
#include <sstream>



Hero::Hero(DungeonCoord* _coord) : Character()
{
	name = "Prince Handsome";
	attackLevel = rand() % 12 + 7;
	defenceLevel = rand() % 12 + 7;
	maxHealth = rand() % 19 + 11;
	currentHealth = maxHealth;
	charisma = rand() % 25 + 10;
	agility = rand() % 25 + 10;
	XP = 0; 
	dungeonCoord = _coord;
} 

std::string Hero::displayInfo()
{
	std::stringstream ss;
	ss << Character::displayInfo();
	ss << "\nAgility: " << agility;
	ss << "\nCharisma: " << charisma;
	if (isBleeding || isPoisoned || isDiseased)
		ss << "\n";
	if (isBleeding)
		ss << "You're bleeding out!   ";
	if (isDiseased)
		ss << "You've got an infection!   ";
	if (isPoisoned)
		ss << "You've been poisoned!";

	return ss.str();
}

std::string Hero::speak()
{
	std::string speak;
	speak = "\n   I will be triumphant!";
	return speak;
}

Hero::~Hero()
{
	//
}
#endif