#ifndef DUNGOBJFACT_C
#define DUNGOBJFACT_C

#include "DungeonObjFactory.h"
#include "DungeonCoord.h"
#include "Item.h"
#include "Weapon.h"
#include "Armour.h"
#include "Monster.h"
#include "Bat.h"
#include "Troll.h"
#include "Toad.h"
#include "Corpse.h"
#include "Warlock.h"

Monster* DungeonObjFactory::getMonster(int _monsterType, DungeonCoord* _coord)
{
	Monster *monster = NULL;
	int randNum = rand() % 50;
	std::string names[50] = { "Lestat", "Uruk-hai", "Chucky", "Dusseldorf", "Gargamel", "Craven", "Freddy", "Pan", "Slasher", "Franko",
		"Sting", "Lyncher", "Brandy", "Tinkerbell", "Waldo", "Ripper", "Scarface", "Diemos", "Hexen", "Ursula", "Raal", "Zephyr", "Heathen",
		"Marzan", "Zoltan", "Franz", "Gruff", "Wheezer", "Snot", "Carrion", "Vulturon", "Tigron", "Maxthon", "Robal", "Azon", "Hazardo",
		"Tyro", "Wrecker", "Rex", "Bloodmoon", "Darksky", "Bash", "Soultorn", "Grave", "Vrak", "Brat", "Xander", "Oberon", "Chanti", "Jamin"
	};
	/*Logic based on monster type*/
	switch ((e_Monster)(_monsterType))
	{
	case BAT:
		monster = new Bat(names[randNum], _coord);
		break;
	case CORPSE:
		monster = new Corpse(names[randNum], _coord);
		break;
	case TOAD:
		monster = new Toad(names[randNum], _coord);
		break;
	case TROLL:
		monster = new Troll(names[randNum], _coord);
		break;
	case WARLOCK:
		monster = new Warlock(names[randNum], _coord);
		break;
	/*case BOSS:
		monster = new Boss(names[randNum], _coord);
		break;*/
	default:
		monster = NULL;
		break;
	}
	return monster;
};

Item* DungeonObjFactory::getItem(int _itemType, int _condition, int _quality, DungeonCoord* _coord)
{
	Item *item = NULL;

	/*Logic based on item type*/
	switch ((e_Item)(_itemType))
	{
		/*case SWORD:
		item = new Sword(_coord, int _condition, int _quality);
		break;
		case SHIELD:
		item = new Shield(_coord, int _condition, int _quality);
		break;
		case AXE:
		item = new Axe(_coord, int _condition, int _quality);
		break;
		case DAGGER:
		item = new Dagger(_coord, int _condition, int _quality);
		break;
		case STAFF:
		item = new Staff(_coord, int _condition, int _quality);
		break;
		case CHAINARMOUR:
		item = new Chainarmour(_coord, int _condition, int _quality);
		break;
		case LEATHERVEST:
		item = new LeatherVest(_coord, int _condition, int _quality);
		break;
		case BOOTS:
		item = new Boots(_coord, int _condition, int _quality);
		break;
		case HELMENT:
		item = new Helmet(_coord, int _condition, int _quality);
		break;
		case TREASURE:
		item = new Treasure(_coord, _quality);
		break;
		case POTION:
		item = new Potion(_coord, _quality);
		break;
		case POTION:
		item = new Amulet(_coord, _quality);
		break;
		*/

	default:
		item = NULL;
		break;
	}
	return item;
};
#endif