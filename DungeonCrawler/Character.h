#ifndef CHARACTER_H
#define CHARACTER_H
#include <string>
#include "DungeonCoord.h"
#include "DungeonObject.h"

class Character : public DungeonObject {
public:
	Character();
	int attack();
	void takeDamage(int _damage);
	virtual std::string displayInfo() = 0;
	virtual std::string speak() = 0;
	std::string getName();
	virtual ~Character();

protected:
	std::string name;
	//character (player and monsters) stats
	int attackLevel, defenceLevel, currentHealth, maxHealth, agility;
};


#endif