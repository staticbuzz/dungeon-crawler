#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <sstream>
#include "DungeonCoord.h"
#include "DungeonObject.h"


class Item : public DungeonObject {
public:
	Item(std::string _desc, DungeonCoord* _coord);
	virtual std::string displayInfo();// = 0;
	std::string getDescription();
	virtual ~Item();

protected:
	std::string description;
	//gold coin value of item
	int worth;
};


#endif